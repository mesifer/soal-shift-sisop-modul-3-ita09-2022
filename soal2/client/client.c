#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080
  
int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;

    char buffer[1024] = {0};
    char login_status[1024] = {0};

    char cmd[100];

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    char uname[20], pw[20], string[100];
    
    printf("1. Register\n2. Login\n");
    scanf("%s", cmd);

    send(sock , cmd , strlen(cmd) , 0 );

    printf("Username : ");
    scanf("%s", uname);
    printf("Password : ");
    scanf("%s", pw);
    strcpy(string, uname);
    strcat(string, ":");
    strcat(string, pw);
    string[strlen(string)] = '\0';
    send(sock , string , strlen(string) , 0 );

    if(cmd[0]=='1'){
       printf("Register Successfully\n");
    }

    else if(cmd[0]=='2'){
        valread = read( sock , login_status, 1024);
        if(login_status[0]=='b'){
            printf("Login Success\n");
        }
        else{
            printf("Login Failed!!\n");
            return 0;
        }   
    }
    else {
        printf("Wrong Command !!\n");
    }
    return 0;
}

