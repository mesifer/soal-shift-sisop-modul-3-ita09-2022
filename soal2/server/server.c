#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#define PORT 8080

int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char cmd[1024] = {0};
    char string[1024] = {0};

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }   

    FILE *fusers, *ffiles, *fptr1, *fptr2;
    valread = read( new_socket , cmd, 1024);
    valread = read( new_socket , string, 1024);
    if(cmd[0]=='r'){
        fusers= fopen("users.txt","a");
        fputs(string, fusers);
        fputs("\n", fusers);
        fflush(fusers);
        fclose(fusers);
    }
    else{
        char line[1024];
        int flag=0;
        FILE* srcFile = fopen("users.txt", "r");
        while (fgets(line , sizeof(line) , srcFile )!= NULL)
        {   
            if (strstr(line , string) != NULL)
                flag=1;
        }

        if(flag)
            send(new_socket , "Success", 8 , 0 );
        else{
            send(new_socket , "Failed", 5 , 0 );
            return 0;
        }
            
    }
   }

