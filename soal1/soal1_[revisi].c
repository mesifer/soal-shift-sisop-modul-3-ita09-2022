#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include <dirent.h> 

pthread_t tid[3]; //inisialisasi array untuk menampung thread dalam kasus ini ada 2 thread
pid_t child;
char path[200];

void bash(char *cmd, char *argv[]){
	int status;
    pid_t child = fork();

    if(child == 0){
        execv(cmd, argv);
    } else while(wait(&status) > 0);
}

void mkrepo(char *namerepo, char *folder){
	snprintf(path, sizeof path, "./%s/%s", folder, namerepo);
	char *argv[] = {"mkdir", path, NULL};
	char *cmd = {"/usr/bin/mkdir"};
	bash(cmd,argv);
}

void unzip(char *namefile, char *folder){
	snprintf(path, sizeof path, "-d%s", folder);
	char *argv[] = {"unzip", namefile, path, NULL};
	char *cmd = "/bin/unzip";
	bash(cmd, argv);
}

void mkfile(char *namefile, char *folder){
	snprintf(path, sizeof path, "./%s/%s", folder, namefile);
	char *argv[] = {"touch", path, NULL};
	char *cmd = "/usr/bin/touch";
	bash(cmd, argv);
}

void zip(char *namefile, char *folder, int flag) {
	char dest[200];
	char path2[200];
	snprintf(dest, sizeof dest, "./%s/hasil.zip", folder);
	snprintf(path, sizeof path, "./%s/%s", folder,namefile);
	snprintf(path2, sizeof path2, "./%s/no.txt", folder);
	
	if (flag == 0) {
		char *argv[] = {"zip", "-P", "mihinomenestbagus", "-r", dest, path, NULL};
		char *cmd = "/bin/zip";
		bash(cmd, argv);
	}
	
	if (flag == 1){
		char *argv[] = {"zip", "-P", "mihinomenestbagus", "-r", dest, path, path2, NULL};
		char *cmd = "/bin/zip";
		bash(cmd, argv);
	}
}	

/* ---- Base64 Encoding/Decoding Table --- */
char b64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/* decodeblock - decode 4 '6-bit' characters into 3 8-bit binary bytes */
void decodeblock(unsigned char in[], char *clrstr) {
  unsigned char out[4];
  out[0] = in[0] << 2 | in[1] >> 4;
  out[1] = in[1] << 4 | in[2] >> 2;
  out[2] = in[2] << 6 | in[3] >> 0;
  out[3] = '\0';
  strncat(clrstr, out, sizeof(out));
}

void b64_decode(char *input, char *result, char *folder) {
  int c, phase, i;
  unsigned char in[4];
  char *p;

  result[0] = '\0';
  phase = 0; i=0;
  while(input[i]) {
    c = (int) input[i];
    if(c == '=') {
      decodeblock(in, result); 
      break;
    }
    p = strchr(b64, c);
    if(p) {
      in[phase] = p - b64;
      phase = (phase + 1) % 4;
      if(phase == 0) {
        decodeblock(in, result);
        in[0]=in[1]=in[2]=in[3]=0;
      }
    }
    i++;
  }
  printf("hasil = %s", result);
  snprintf(path, sizeof path, "./%s/file.txt", folder);

	FILE *data;
	data = fopen(path, "a+");
	fprintf(data, "%s\n", clrdst);
}

void mvfile(char *folder){
	char pathto[200];
	mkrepo("hasil", folder);
	snprintf(path, sizeof path, "./%s/file.txt", folder);
	snprintf(pathto, sizeof pathto, "./%s/hasil", folder);
	char *argv[] = {"mv", path, pathto, NULL};
	char *cmd = {"/usr/bin/mv"};
	bash(cmd, argv);
}


int decode(char *folder, char *ext){
	for (int i = 1; i <= 9; i++)
	{
		char c[1000];
		char result[1000];
		snprintf(path, sizeof path, "./%s/%s%d.txt",folder,ext,i);
		FILE *fptr;
		if ((fptr = fopen(path, "r")) == NULL) {
			printf("Error! File cannot be opened.");
			exit(1);
		}

		fscanf(fptr, "%[^\n]", c);
		printf("\nData from the file: %s", c);
		
		b64_decode(c,result,folder);
	
		fclose(fptr);
	}
	
}


void* mainThread()
{
	char *argv1[] = {"clear", NULL};
	pthread_t id=pthread_self();
	if(pthread_equal(id,tid[0])) //thread untuk clear layar
	{
		child = fork();
		if (child==0) {
		    execv("/usr/bin/clear", argv1);
	    }
	}
	else if(pthread_equal(id,tid[1])) // thread unzip music.zip
	{
			char *folder;
			folder = "music";
			unzip("music.zip", folder);
			decode(folder,"m");
			mvfile(folder);
			zip("hasil",folder,0);
			unzip("./music/hasil.zip", folder);
			mkfile("no.txt",folder);
			zip("hasil",folder,1);
			
			fflush(stdout);
			sleep(1);
	}
	else if(pthread_equal(id,tid[2])) // thread unzip quote
	{
        child = fork();
        if (child==0) {
				char *folder;
				folder = "quote";
	
				unzip("quote.zip",folder);
				decode(folder,"q");
				mvfile(folder);
				zip("hasil",folder,0);
				unzip("./quote/hasil.zip",folder);
				mkfile("no.txt",folder);
				zip("hasil",folder,1);
						
				fflush(stdout);
				sleep(1);
	    }
	}

	return NULL;
}

int main(void)
{
	int i=0;
	int err;
	while(i<3) // loop sejumlah thread
	{
		err=pthread_create(&(tid[i]),NULL,&mainThread,NULL); //membuat thread
		if(err!=0) //cek error
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
		i++;
	}
	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);

	
	exit(0);
	return 0;
}
