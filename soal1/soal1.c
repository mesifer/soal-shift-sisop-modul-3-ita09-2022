#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include <dirent.h>
#define BUZZ_SIZE 1024

pthread_t tid[3]; 
pid_t child;
char path[200];

void bash(char *cmd, char *argv[]){
	int status;
    pid_t child = fork();

    if(child == 0){
        execv(cmd, argv);
    } else while(wait(&status) > 0);
}

void mkrepo(char *namerepo){
	char *argv[] = {"mkdir", namerepo, NULL};
	char *cmd = {"/usr/bin/mkdir"};
	bash(cmd,argv);
}

void unzip(char *namefile, char *folder){
	snprintf(path, sizeof path, "-d%s", folder);
	char *argv[] = {"unzip", namefile, path, NULL};
	char *cmd = "/bin/unzip";
	bash(cmd, argv);
}

int decode(char *folder, char *ext){

	DIR *repo;
		struct dirent *dir;
		snprintf(path, sizeof path, "./%s",folder);
		repo = opendir(path);	
		if (repo) {

			while ((dir = readdir(repo)) != NULL){

					if(dir->d_type==DT_REG){
								// mkdir("file.txt")
								printf("%s\n",dir->d_name);
								char decode[200];
								char *filename = dir->d_name;
								char *result;
								result = strstr(dir->d_name,"file.txt");
								// if (!result)
								// {
									snprintf(decode, sizeof decode, "music/%s", filename);
									char *argv[] = {"base64 -d ", decode, NULL};
									char *cmd = "/usr/bin/base64";
									bash(cmd,argv);
									printf("%s\n",decode);
							
								
								
						}
					}

			}
}


void* playandcount(void *arg)
{
	char *argv1[] = {"clear", NULL};
	char *argv2[] = {"xlogo", NULL};
	unsigned long i=0;
	pthread_t id=pthread_self();
	int iter;
	if(pthread_equal(id,tid[0])) //thread untuk clear layar
	{
		child = fork();
		if (child==0) {
		    execv("/usr/bin/clear", argv1);
	    }
	}
	else if(pthread_equal(id,tid[1])) // thread  unzip music.zip
	{
			char *folder;
			folder = "music";
			unzip("music.zip", folder);

			fflush(stdout);
			sleep(1);
	}
	else if(pthread_equal(id,tid[2])) // thread unzip quote
	{
        child = fork();
        if (child==0) {
				char *folder;
				folder = "quote";

				unzip("quote.zip",folder);

				fflush(stdout);
				sleep(1);
	    }
	}

	return NULL;
}

int main(void)
{
	int i=0;
	int err;
	while(i<3) // loop sejumlah thread
	{
		err=pthread_create(&(tid[i]),NULL,&playandcount,NULL); //membuat thread
		if(err!=0) //cek error
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
		i++;
	}
	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);

	decode("music","m");

	exit(0);
	return 0;
}
