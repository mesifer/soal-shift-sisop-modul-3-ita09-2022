# soal-shift-sisop-modul-3-ita09-2022

| No | Nama | NRP |
| --- | --- | --- |
| 1 | Bagus Ridho R | 5027201043 |
| 2 | Fairuz Azhar A | 5027201059 |
| 3 | Rafael Nixon | 05311940000025 |

# Soal 1
## main fungsi dalam thread `mainThread`
```c
pthread_t tid[3];

void* mainThread()
{
	char *argv1[] = {"clear", NULL};
	pthread_t id=pthread_self();
	if(pthread_equal(id,tid[0])) //thread untuk clear layar
	{
		child = fork();
		if (child==0) {
		    execv("/usr/bin/clear", argv1);
	    }
	}
	else if(pthread_equal(id,tid[1])) // thread unzip music.zip
	{
		char *folder;
		folder = "music";
		unzip("music.zip", folder);
		decode(folder,"m");
		mvfile(folder);
		zip("hasil",folder,0);
		unzip("./music/hasil.zip", folder);
		mkfile("no.txt",folder);
		zip("hasil",folder,1);
			
		fflush(stdout);
		sleep(1);
	}
	else if(pthread_equal(id,tid[2])) // thread unzip quote
	{
        child = fork();
        if (child==0) {
			char *folder;
			folder = "quote";
	
			unzip("quote.zip",folder);
			decode(folder,"q");
			mvfile(folder);
			zip("hasil",folder,0);
			unzip("./quote/hasil.zip",folder);
			mkfile("no.txt",folder);
			zip("hasil",folder,1);
						
			fflush(stdout);
			sleep(1);
	    }
	}
	return NULL;
}
```
### Penjelasan
1. Membuat thread `pthread_t` sebanyak 3 (clear layar, unzip music.zip, unzip quote.zip)
2. Menggunakan percabangan `if else` untuk menjalankan `pthread_t`
3. Main fungsi berupa (unzip, decode, mvfile, dan zip)

## main fungsi dalam fungsi `bash`
```c
void bash(char *cmd, char *argv[]){
    int status;
    pid_t child = fork();

    if(child == 0){
        execv(cmd, argv);
    } else while(wait(&status) > 0);
}
```
## main fungsi dalam fungsi `decode`
*revisi soal 1
perubahan pada fungsi decode, dimana semula menggunakan execv base64, diganti dengan fungsi decode menggunakan table decoding base64.
#### awal
```c
DIR *repo;
struct dirent *dir;
snprintf(path, sizeof path, "./%s",folder);
repo = opendir(path);	
if (repo) {
	while ((dir = readdir(repo)) != NULL){
		if(dir->d_type==DT_REG){
			printf("%s\n",dir->d_name);
			char decode[200];
			char *filename = dir->d_name;
			char *result;
			result = strstr(dir->d_name,"file.txt");
						
			snprintf(decode, sizeof decode, "music/%s", filename);
			char *argv[] = {"base64 -d ", decode, NULL};
			char *cmd = "/usr/bin/base64";
			bash(cmd,argv);
			printf("%s\n",decode);
		}
	}
}
```
#### revisi
```c
char b64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/* decodeblock - decode 4 '6-bit' characters into 3 8-bit binary bytes */
void decodeblock(unsigned char in[], char *clrstr) {
  unsigned char out[4];
  out[0] = in[0] << 2 | in[1] >> 4;
  out[1] = in[1] << 4 | in[2] >> 2;
  out[2] = in[2] << 6 | in[3] >> 0;
  out[3] = '\0';
  strncat(clrstr, out, sizeof(out));
}

void b64_decode(char *input, char *result, char *folder) {
  int c, phase, i;
  unsigned char in[4];
  char *p;

  result[0] = '\0';
  while(input[i]) {
  phase = 0; i=0;
    c = (int) input[i];
    if(c == '=') {
      decodeblock(in, result); 
      break;
    }
    p = strchr(b64, c);
    if(p) {
      in[phase] = p - b64;
      phase = (phase + 1) % 4;
      if(phase == 0) {
        decodeblock(in, result);
        in[0]=in[1]=in[2]=in[3]=0;
      }
    }
    i++;
  }
  printf("hasil = %s", result);
  snprintf(path, sizeof path, "./%s/file.txt", folder);

	FILE *data;
	data = fopen(path, "a+");
	fprintf(data, "%s\n", result);
 
}


int decode(char *folder, char *ext){
	for (int i = 1; i <= 9; i++)
	{
		char c[1000];
		char result[1000];
		snprintf(path, sizeof path, "./%s/%s%d.txt",folder,ext,i);
		FILE *fptr;
		if ((fptr = fopen(path, "r")) == NULL) {
			printf("Error! File cannot be opened.");
			// Program exits if the file pointer returns NULL.
			exit(1);
		}

		// reads text until newline is encountered
		fscanf(fptr, "%[^\n]", c);
		printf("\nData from the file: %s", c);
		snprintf(path, sizeof path, "./%s/file.txt", folder);
		

		// b64_decode(c,result,folder);
		b64_decode(c,result,folder);
		fclose(fptr);
	}
	
}
```
### Dan penambahan fungsi `zip`, `mvfile` untuk men-zip folder `hasil` dan memindah file `file.txt` 
#### Zip
```c
void zip(char *namefile, char *folder, int flag) {
	char dest[200];
	char path2[200];
	snprintf(dest, sizeof dest, "./%s/hasil.zip", folder);
	snprintf(path, sizeof path, "./%s/%s", folder,namefile);
	snprintf(path2, sizeof path2, "./%s/no.txt", folder);
	
	if (flag == 0) {
		char *argv[] = {"zip", "-P", "mihinomenestbagus", "-r", dest, path, NULL};
		char *cmd = "/bin/zip";
		bash(cmd, argv);
	}
	
	if (flag == 1){
		char *argv[] = {"zip", "-P", "mihinomenestbagus", "-r", dest, path, path2, NULL};
		char *cmd = "/bin/zip";
		bash(cmd, argv);
	}
	
	
}	
```
#### mvfile
```c
void mvfile(char *folder){
	char pathto[200];
	mkrepo("hasil", folder);
	snprintf(path, sizeof path, "./%s/file.txt", folder);
	snprintf(pathto, sizeof pathto, "./%s/hasil", folder);
	char *argv[] = {"mv", path, pathto, NULL};
	char *cmd = {"/usr/bin/mv"};
	bash(cmd, argv);
}
```
## hasil revisi
![image](img/SS_revisi.jpg)
### Penjelasan
1. Default folder berada di ../soal1/
2. Di dalam folder tersebut membuat repo `music` dan `quote` masing-masing berisi file hasil unzip `music.zip` dan `quote.zip`
3. Kemudian file `.txt` di-encoding dan hasil encoding diletakkan di /hasil/file.txt
4. Kemudian folder `hasil` di-zip dan diberi password `mihinomenestbagus`
```c
char *argv[] = {"zip", "-P", "mihinomenestbagus", "-r", dest, path, NULL};
char *cmd = "/bin/zip";
bash(cmd, argv);
```
5. Lalu, di-unzip kembali dan membuat file `no.txt` kemudian mengunzip kedua file tersebut dengan password yang sama



# Soal 2

Revisi

## Konfigurasi `Server`
```c
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#define PORT 8080

int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char cmd[1024] = {0};
    char string[1024] = {0};

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }   

    FILE *fusers, *ffiles, *fptr1, *fptr2;
    valread = read( new_socket , cmd, 1024);
    valread = read( new_socket , string, 1024);
    if(cmd[0]=='r'){
        fusers= fopen("users.txt","a");
        fputs(string, fusers);
        fputs("\n", fusers);
        fflush(fusers);
        fclose(fusers);
    }
    else{
        char line[1024];
        int flag=0;
        FILE* srcFile = fopen("users.txt", "r");
        while (fgets(line , sizeof(line) , srcFile )!= NULL)
        {   
            if (strstr(line , string) != NULL)
                flag=1;
        }

        if(flag)
            send(new_socket , "Success", 8 , 0 );
        else{
            send(new_socket , "Failed", 5 , 0 );
            return 0;
        }
            
    }
   }
```
## Konfigurasi `Client`
```c
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080
  
int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;

    char buffer[1024] = {0};
    char login_status[1024] = {0};

    char cmd[100];

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    char uname[20], pw[20], string[100];
    
    printf("1. Register\n2. Login\n");
    scanf("%s", cmd);

    send(sock , cmd , strlen(cmd) , 0 );

    printf("Username : ");
    scanf("%s", uname);
    printf("Password : ");
    scanf("%s", pw);
    strcpy(string, uname);
    strcat(string, ":");
    strcat(string, pw);
    string[strlen(string)] = '\0';
    send(sock , string , strlen(string) , 0 );

    if(cmd[0]=='1'){
       printf("Register Successfully\n");
    }

    else if(cmd[0]=='2'){
        valread = read( sock , login_status, 1024);
        if(login_status[0]=='b'){
            printf("Login Success\n");
        }
        else{
            printf("Login Failed!!\n");
            return 0;
        }   
    }
    else {
        printf("Wrong Command !!\n");
    }
    return 0;
}
```
## Hasil
![image](img/SS_soal2.jpg)








# Soal no 3

Revisi 

## Installation

Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya.

a,b, dan c) dengan itu mengkategorikand dengan cara memfilter seluruh file pada directory dengan rekrusif, pengkategorian dilakukan dengan cara memasukan file ke dalam directory ke ektensi file yang sudah tertara (dengan file unknown dimasukan ke dalam directory unkown, dan file hidden dimasukan ke dalam directory hidden). Agar proses kategori berjalan lebih cepat, tiap 1 file yang dikategorikan dioperasikan oleh 1 thread

```bash
#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include <sys/stat.h>
#include<dirent.h>
#include<libgen.h>
pthread_mutex_t bufferlock;

typedef struct file{
    char DIR[1024];
    char filename[1024];
}file_t;

//menjadikan nya ke lowercase
void tolowerstr(char* str){
    for(int i = 0 ; i < strlen(str);i++){
        if(str[i]<=90 && str[i]>=65){
            str[i]+=32;
        }else{
            continue;
        }
    }
}

//medapatkan file
char* filename_ext(char *filename){
    printf("jalan1 %s\n",filename);
    char* extname = (char*)malloc(sizeof(char)*(PATH_MAX + PATH_MAX));
    memset(extname,'\0',sizeof(char)*(PATH_MAX + PATH_MAX));
    char* dot = strchr(filename,'.');
    char *dotted=strrchr(filename,'/');
    if(dotted[1]=='.'){
        strcpy(extname,"Hidden");
        return extname;
    }else if(dot == NULL){
        strcpy(extname,"Unknown");
        return extname;
    }else{
        strcpy(extname,dot+1);
        tolowerstr(extname);
        return extname;
    }
}

//mengfilter kategori
void* movetofolder(void* args){
    file_t* filenow = (file_t*)args;
    printf("jalan1 %s\n",filenow->filename);
    char* extname=filename_ext(filenow->filename);
    printf("jalan %s\n",filenow->filename);
    char* pathname = (char*)malloc(sizeof(char)*(PATH_MAX + PATH_MAX));
    memset(pathname,0,sizeof(char)*(PATH_MAX + PATH_MAX));
    strcpy(pathname,filenow->DIR);
    strcat(pathname,"/hartakarunans/");
    strcat(pathname,extname);
    mkdir(pathname,0777);
    pthread_mutex_lock(&bufferlock);
    strcat(pathname,"/");
    char newpath[(PATH_MAX + PATH_MAX)];
    memset(newpath,0,sizeof(newpath));
    strcpy(newpath,pathname);
    strcat(newpath,basename(filenow->filename));
    printf("moving %s to %s\n",filenow->filename,newpath);
    rename(filenow->filename,newpath);
    pthread_mutex_unlock(&bufferlock);
}

int it=0;

void filterkategori (char* base, pthread_t *thread){
    char path[1024];
    struct dirent *dp;
    DIR *dir = opendir(base);
    if(!dir) return;
    while ((dp = readdir(dir))!=NULL){
        if(strcmp(dp->d_name,".")!=0 && strcmp(dp->d_name,"..")!=0){
            strcpy(path,base);
            strcat(path,"/");
            strcat(path,dp->d_name);
            file_t* filenow=(file_t*)malloc(sizeof(file_t));
            strcpy(filenow->filename,path);
            printf("%s\n",filenow->filename);
            strcpy(filenow->DIR,"/home/iyuz/shift3");
            if(strcmp(path,"/home/iyuz/shift3")!=0){
                pthread_create(&thread[it],NULL,movetofolder,(void*)filenow);
                sleep(1);
                it++;
            }

        }
    }
    closedir(dir);
}


int main(int argc, char * argv[]){
    char curDir[2048];
    getcwd(curDir,sizeof(curDir));
    pthread_mutex_init(&bufferlock,NULL);
    pthread_t copy_thread[1000];
    char renamefolder[2048];
    strcpy(renamefolder,curDir);
    mkdir("hartakarunans",07770);
    strcat(curDir,"/hartakarun");
    DIR *d;
    struct dirent *dir;
    struct dirent *dp;
    d=opendir(curDir);
    if(d){
        filterkategori(curDir,copy_thread);
    }
    closedir(d);
    d=opendir(".");
    if(d){
        while((dp=readdir(d))!=NULL){
            if(strcmp(dp->d_name,".")!=0 && strcmp(dp->d_name,"..")!=0){
                if(dp->d_type==DT_REG){
                    continue;
                }else{
                    printf("%s\n",dp->d_name);
                    if(strcmp(dp->d_name,"hartakarunans")==0){
                        printf("jalan");
                        rename(dp->d_name,"client/hartakarun");
                    }
                }
            }
        }
    }
    closedir(d);
}
```

diatas merupakan program yang akan membuat directory dummy untuk menyimpan hasil dari dari program pengkategorian filter dan akan di ganti dengan nama hartakarun ketika pengkategorian selesai, dan akan memanggil fungsi, Pada fungsi kategoriin,akan membuat sebuah thread untuk mengkategorikan file.

